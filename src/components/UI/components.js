import ButtonSmall from "@/components/UI/buttons/ButtonSmall";
import ButtonMiddle from "@/components/UI/buttons/ButtonMiddle";
import ButtonLarge from "@/components/UI/buttons/ButtonLarge";
import Divider from "@/components/UI/Divider";
import ButtonBase from "@/components/UI/buttons/ButtonBase";
import ToggleButton from "@/components/UI/buttons/ToggleButton";
import FormInput from "@/components/UI/forms/FormInput";
import FormTextarea from "@/components/UI/forms/FormTextarea";

export default [
    ButtonBase,
    ButtonSmall,
    ButtonMiddle,
    ButtonLarge,
    ToggleButton,
    FormInput,
    FormTextarea,
    Divider
]