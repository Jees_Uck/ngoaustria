import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/news',
    name: 'News',
    component: () => import('../views/News')
  },
  {
    path: '/team',
    name: 'Team',
    component: () => import('../views/Team')
  },
  {
    path: '/support',
    name: 'Support',
    component: () => import('../views/Support')
  },
  {
    path: '/gallery',
    name: 'Gallery',
    component: () => import('../views/Gallery')
  },
  // {
  //   path: '/posts/:id',
  //   name: 'Posts',
  //   component: () => import('../views/PostListItemPage')
  // },

]

const router = createRouter({
  history: createWebHistory(),
  linkActiveClass: 'active',
  routes
})

export default router
