import { createApp } from 'vue'
import App from './App.vue'
import router from './router/router'
import store from './store'
import dayjs from 'dayjs';
import VueLazyLoad from 'vue3-lazyload'
import components from "@/components/UI/components";

const app = createApp(App)

components.forEach(component => {
    app.component(component.name, component)
})

app
    .use(router)
    .use(store)
    .use(dayjs)
    .use(VueLazyLoad)
    .mount('#app');